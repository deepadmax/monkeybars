# Monkeybars

Monkeybars is a specification for virtual reality worlds akin to websites.

It is based on the same ideas as regular web browsers;

- [A markup language](./markup.md) for describing the structure of the virtual world,
- A stylesheet language for how objects look and behave,
- and a scripting language for anything custom that cannot be achieved by the former two.

**This is purely an idea for the time being. It is uncertain whether one such VR space browser will be implemented.**