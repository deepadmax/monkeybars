# Monkeybars Markup Language (MBML)

**MBML** has identical syntax to that of HTML. This is both so that it resembles the markup of the web *and* so that current HTML parsers can be used in implementations.

## Tags

### Object

The `<object>` tag is akin to `<div>` in HTML. It is used for any kind of object and it'll be of what most objects are made.

```html
<object class="candle">
```

### Light

The `<light>` tag defines a light source.

```html
<light>
```

### Portal

The `<portal>` tag is akin to `<a>` in HTML.

```html
<portal ref="library.mbml">
```